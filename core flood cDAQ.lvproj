﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="18008000">
	<Property Name="SMProvider.SMVersion" Type="Int">201310</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="alias.value" Type="Str">10.0.0.1</Property>
		<Property Name="CCSymbols" Type="Str"></Property>
		<Property Name="IOScan.Faults" Type="Str"></Property>
		<Property Name="IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="IOScan.Period" Type="UInt">10000</Property>
		<Property Name="IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="IOScan.Priority" Type="UInt">9</Property>
		<Property Name="IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="IOScan.StartEngineOnDeploy" Type="Bool">false</Property>
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.acl" Type="Str">0800000008000000</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str"></Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.access" Type="Str"></Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.viscripting.showScriptingOperationsInContextHelp" Type="Bool">false</Property>
		<Property Name="server.viscripting.showScriptingOperationsInEditor" Type="Bool">false</Property>
		<Property Name="specify.custom.address" Type="Bool">true</Property>
		<Item Name="controls" Type="Folder" URL="../controls">
			<Property Name="NI.DISK" Type="Bool">true</Property>
		</Item>
		<Item Name="documentation" Type="Folder" URL="../documentation">
			<Property Name="NI.DISK" Type="Bool">true</Property>
		</Item>
		<Item Name="subVI" Type="Folder" URL="../subVI">
			<Property Name="NI.DISK" Type="Bool">true</Property>
		</Item>
		<Item Name="logos" Type="Folder"/>
		<Item Name="diagnostic" Type="Folder" URL="../diagnostic">
			<Property Name="NI.DISK" Type="Bool">true</Property>
		</Item>
		<Item Name="daqmx stuff" Type="Folder">
			<Item Name="CurrentTask" Type="NI-DAQmx Task">
				<Property Name="\0\AI.ADCTimingMode" Type="Str">High Resolution</Property>
				<Property Name="\0\AI.Current.Units" Type="Str">Amps</Property>
				<Property Name="\0\AI.CurrentShunt.Loc" Type="Str">Internal</Property>
				<Property Name="\0\AI.CurrentShunt.Resistance" Type="Str">34.009999999999998</Property>
				<Property Name="\0\AI.Max" Type="Str">0.021999999999999999</Property>
				<Property Name="\0\AI.MeasType" Type="Str">Current</Property>
				<Property Name="\0\AI.Min" Type="Str">0</Property>
				<Property Name="\0\AI.TermCfg" Type="Str">RSE</Property>
				<Property Name="\0\ChanType" Type="Str">Analog Input</Property>
				<Property Name="\0\Name" Type="Str">CurrentTask/Current_0</Property>
				<Property Name="\0\PhysicalChanName" Type="Str">cDAQ-phase2012Mod3/ai0</Property>
				<Property Name="\1\AI.ADCTimingMode" Type="Str">High Resolution</Property>
				<Property Name="\1\AI.Current.Units" Type="Str">Amps</Property>
				<Property Name="\1\AI.CurrentShunt.Loc" Type="Str">Internal</Property>
				<Property Name="\1\AI.CurrentShunt.Resistance" Type="Str">34.009999999999998</Property>
				<Property Name="\1\AI.Max" Type="Str">0.021999999999999999</Property>
				<Property Name="\1\AI.MeasType" Type="Str">Current</Property>
				<Property Name="\1\AI.Min" Type="Str">0</Property>
				<Property Name="\1\AI.TermCfg" Type="Str">RSE</Property>
				<Property Name="\1\ChanType" Type="Str">Analog Input</Property>
				<Property Name="\1\Name" Type="Str">CurrentTask/Current_1</Property>
				<Property Name="\1\PhysicalChanName" Type="Str">cDAQ-phase2012Mod3/ai1</Property>
				<Property Name="\10\AI.ADCTimingMode" Type="Str">High Resolution</Property>
				<Property Name="\10\AI.Current.Units" Type="Str">Amps</Property>
				<Property Name="\10\AI.CurrentShunt.Loc" Type="Str">Internal</Property>
				<Property Name="\10\AI.CurrentShunt.Resistance" Type="Str">34.009999999999998</Property>
				<Property Name="\10\AI.Max" Type="Str">0.021999999999999999</Property>
				<Property Name="\10\AI.MeasType" Type="Str">Current</Property>
				<Property Name="\10\AI.Min" Type="Str">0</Property>
				<Property Name="\10\AI.TermCfg" Type="Str">RSE</Property>
				<Property Name="\10\ChanType" Type="Str">Analog Input</Property>
				<Property Name="\10\Name" Type="Str">CurrentTask/Current_10</Property>
				<Property Name="\10\PhysicalChanName" Type="Str">cDAQ-phase2012Mod3/ai10</Property>
				<Property Name="\11\AI.ADCTimingMode" Type="Str">High Resolution</Property>
				<Property Name="\11\AI.Current.Units" Type="Str">Amps</Property>
				<Property Name="\11\AI.CurrentShunt.Loc" Type="Str">Internal</Property>
				<Property Name="\11\AI.CurrentShunt.Resistance" Type="Str">34.009999999999998</Property>
				<Property Name="\11\AI.Max" Type="Str">0.021999999999999999</Property>
				<Property Name="\11\AI.MeasType" Type="Str">Current</Property>
				<Property Name="\11\AI.Min" Type="Str">0</Property>
				<Property Name="\11\AI.TermCfg" Type="Str">RSE</Property>
				<Property Name="\11\ChanType" Type="Str">Analog Input</Property>
				<Property Name="\11\Name" Type="Str">CurrentTask/Current_11</Property>
				<Property Name="\11\PhysicalChanName" Type="Str">cDAQ-phase2012Mod3/ai11</Property>
				<Property Name="\12\AI.ADCTimingMode" Type="Str">High Resolution</Property>
				<Property Name="\12\AI.Current.Units" Type="Str">Amps</Property>
				<Property Name="\12\AI.CurrentShunt.Loc" Type="Str">Internal</Property>
				<Property Name="\12\AI.CurrentShunt.Resistance" Type="Str">34.009999999999998</Property>
				<Property Name="\12\AI.Max" Type="Str">0.021999999999999999</Property>
				<Property Name="\12\AI.MeasType" Type="Str">Current</Property>
				<Property Name="\12\AI.Min" Type="Str">0</Property>
				<Property Name="\12\AI.TermCfg" Type="Str">RSE</Property>
				<Property Name="\12\ChanType" Type="Str">Analog Input</Property>
				<Property Name="\12\Name" Type="Str">CurrentTask/Current_12</Property>
				<Property Name="\12\PhysicalChanName" Type="Str">cDAQ-phase2012Mod3/ai12</Property>
				<Property Name="\13\AI.ADCTimingMode" Type="Str">High Resolution</Property>
				<Property Name="\13\AI.Current.Units" Type="Str">Amps</Property>
				<Property Name="\13\AI.CurrentShunt.Loc" Type="Str">Internal</Property>
				<Property Name="\13\AI.CurrentShunt.Resistance" Type="Str">34.009999999999998</Property>
				<Property Name="\13\AI.Max" Type="Str">0.021999999999999999</Property>
				<Property Name="\13\AI.MeasType" Type="Str">Current</Property>
				<Property Name="\13\AI.Min" Type="Str">0</Property>
				<Property Name="\13\AI.TermCfg" Type="Str">RSE</Property>
				<Property Name="\13\ChanType" Type="Str">Analog Input</Property>
				<Property Name="\13\Name" Type="Str">CurrentTask/Current_13</Property>
				<Property Name="\13\PhysicalChanName" Type="Str">cDAQ-phase2012Mod3/ai13</Property>
				<Property Name="\14\AI.ADCTimingMode" Type="Str">High Resolution</Property>
				<Property Name="\14\AI.Current.Units" Type="Str">Amps</Property>
				<Property Name="\14\AI.CurrentShunt.Loc" Type="Str">Internal</Property>
				<Property Name="\14\AI.CurrentShunt.Resistance" Type="Str">34.009999999999998</Property>
				<Property Name="\14\AI.Max" Type="Str">0.021999999999999999</Property>
				<Property Name="\14\AI.MeasType" Type="Str">Current</Property>
				<Property Name="\14\AI.Min" Type="Str">0</Property>
				<Property Name="\14\AI.TermCfg" Type="Str">RSE</Property>
				<Property Name="\14\ChanType" Type="Str">Analog Input</Property>
				<Property Name="\14\Name" Type="Str">CurrentTask/Current_14</Property>
				<Property Name="\14\PhysicalChanName" Type="Str">cDAQ-phase2012Mod3/ai14</Property>
				<Property Name="\15\AI.ADCTimingMode" Type="Str">High Resolution</Property>
				<Property Name="\15\AI.Current.Units" Type="Str">Amps</Property>
				<Property Name="\15\AI.CurrentShunt.Loc" Type="Str">Internal</Property>
				<Property Name="\15\AI.CurrentShunt.Resistance" Type="Str">34.009999999999998</Property>
				<Property Name="\15\AI.Max" Type="Str">0.021999999999999999</Property>
				<Property Name="\15\AI.MeasType" Type="Str">Current</Property>
				<Property Name="\15\AI.Min" Type="Str">0</Property>
				<Property Name="\15\AI.TermCfg" Type="Str">RSE</Property>
				<Property Name="\15\ChanType" Type="Str">Analog Input</Property>
				<Property Name="\15\Name" Type="Str">CurrentTask/Current_15</Property>
				<Property Name="\15\PhysicalChanName" Type="Str">cDAQ-phase2012Mod3/ai15</Property>
				<Property Name="\2\AI.ADCTimingMode" Type="Str">High Resolution</Property>
				<Property Name="\2\AI.Current.Units" Type="Str">Amps</Property>
				<Property Name="\2\AI.CurrentShunt.Loc" Type="Str">Internal</Property>
				<Property Name="\2\AI.CurrentShunt.Resistance" Type="Str">34.009999999999998</Property>
				<Property Name="\2\AI.Max" Type="Str">0.021999999999999999</Property>
				<Property Name="\2\AI.MeasType" Type="Str">Current</Property>
				<Property Name="\2\AI.Min" Type="Str">0</Property>
				<Property Name="\2\AI.TermCfg" Type="Str">RSE</Property>
				<Property Name="\2\ChanType" Type="Str">Analog Input</Property>
				<Property Name="\2\Name" Type="Str">CurrentTask/Current_2</Property>
				<Property Name="\2\PhysicalChanName" Type="Str">cDAQ-phase2012Mod3/ai2</Property>
				<Property Name="\3\AI.ADCTimingMode" Type="Str">High Resolution</Property>
				<Property Name="\3\AI.Current.Units" Type="Str">Amps</Property>
				<Property Name="\3\AI.CurrentShunt.Loc" Type="Str">Internal</Property>
				<Property Name="\3\AI.CurrentShunt.Resistance" Type="Str">34.009999999999998</Property>
				<Property Name="\3\AI.Max" Type="Str">0.021999999999999999</Property>
				<Property Name="\3\AI.MeasType" Type="Str">Current</Property>
				<Property Name="\3\AI.Min" Type="Str">0</Property>
				<Property Name="\3\AI.TermCfg" Type="Str">RSE</Property>
				<Property Name="\3\ChanType" Type="Str">Analog Input</Property>
				<Property Name="\3\Name" Type="Str">CurrentTask/Current_3</Property>
				<Property Name="\3\PhysicalChanName" Type="Str">cDAQ-phase2012Mod3/ai3</Property>
				<Property Name="\4\AI.ADCTimingMode" Type="Str">High Resolution</Property>
				<Property Name="\4\AI.Current.Units" Type="Str">Amps</Property>
				<Property Name="\4\AI.CurrentShunt.Loc" Type="Str">Internal</Property>
				<Property Name="\4\AI.CurrentShunt.Resistance" Type="Str">34.009999999999998</Property>
				<Property Name="\4\AI.Max" Type="Str">0.021999999999999999</Property>
				<Property Name="\4\AI.MeasType" Type="Str">Current</Property>
				<Property Name="\4\AI.Min" Type="Str">0</Property>
				<Property Name="\4\AI.TermCfg" Type="Str">RSE</Property>
				<Property Name="\4\ChanType" Type="Str">Analog Input</Property>
				<Property Name="\4\Name" Type="Str">CurrentTask/Current_4</Property>
				<Property Name="\4\PhysicalChanName" Type="Str">cDAQ-phase2012Mod3/ai4</Property>
				<Property Name="\5\AI.ADCTimingMode" Type="Str">High Resolution</Property>
				<Property Name="\5\AI.Current.Units" Type="Str">Amps</Property>
				<Property Name="\5\AI.CurrentShunt.Loc" Type="Str">Internal</Property>
				<Property Name="\5\AI.CurrentShunt.Resistance" Type="Str">34.009999999999998</Property>
				<Property Name="\5\AI.Max" Type="Str">0.021999999999999999</Property>
				<Property Name="\5\AI.MeasType" Type="Str">Current</Property>
				<Property Name="\5\AI.Min" Type="Str">0</Property>
				<Property Name="\5\AI.TermCfg" Type="Str">RSE</Property>
				<Property Name="\5\ChanType" Type="Str">Analog Input</Property>
				<Property Name="\5\Name" Type="Str">CurrentTask/Current_5</Property>
				<Property Name="\5\PhysicalChanName" Type="Str">cDAQ-phase2012Mod3/ai5</Property>
				<Property Name="\6\AI.ADCTimingMode" Type="Str">High Resolution</Property>
				<Property Name="\6\AI.Current.Units" Type="Str">Amps</Property>
				<Property Name="\6\AI.CurrentShunt.Loc" Type="Str">Internal</Property>
				<Property Name="\6\AI.CurrentShunt.Resistance" Type="Str">34.009999999999998</Property>
				<Property Name="\6\AI.Max" Type="Str">0.021999999999999999</Property>
				<Property Name="\6\AI.MeasType" Type="Str">Current</Property>
				<Property Name="\6\AI.Min" Type="Str">0</Property>
				<Property Name="\6\AI.TermCfg" Type="Str">RSE</Property>
				<Property Name="\6\ChanType" Type="Str">Analog Input</Property>
				<Property Name="\6\Name" Type="Str">CurrentTask/Current_6</Property>
				<Property Name="\6\PhysicalChanName" Type="Str">cDAQ-phase2012Mod3/ai6</Property>
				<Property Name="\7\AI.ADCTimingMode" Type="Str">High Resolution</Property>
				<Property Name="\7\AI.Current.Units" Type="Str">Amps</Property>
				<Property Name="\7\AI.CurrentShunt.Loc" Type="Str">Internal</Property>
				<Property Name="\7\AI.CurrentShunt.Resistance" Type="Str">34.009999999999998</Property>
				<Property Name="\7\AI.Max" Type="Str">0.021999999999999999</Property>
				<Property Name="\7\AI.MeasType" Type="Str">Current</Property>
				<Property Name="\7\AI.Min" Type="Str">0</Property>
				<Property Name="\7\AI.TermCfg" Type="Str">RSE</Property>
				<Property Name="\7\ChanType" Type="Str">Analog Input</Property>
				<Property Name="\7\Name" Type="Str">CurrentTask/Current_7</Property>
				<Property Name="\7\PhysicalChanName" Type="Str">cDAQ-phase2012Mod3/ai7</Property>
				<Property Name="\8\AI.ADCTimingMode" Type="Str">High Resolution</Property>
				<Property Name="\8\AI.Current.Units" Type="Str">Amps</Property>
				<Property Name="\8\AI.CurrentShunt.Loc" Type="Str">Internal</Property>
				<Property Name="\8\AI.CurrentShunt.Resistance" Type="Str">34.009999999999998</Property>
				<Property Name="\8\AI.Max" Type="Str">0.021999999999999999</Property>
				<Property Name="\8\AI.MeasType" Type="Str">Current</Property>
				<Property Name="\8\AI.Min" Type="Str">0</Property>
				<Property Name="\8\AI.TermCfg" Type="Str">RSE</Property>
				<Property Name="\8\ChanType" Type="Str">Analog Input</Property>
				<Property Name="\8\Name" Type="Str">CurrentTask/Current_8</Property>
				<Property Name="\8\PhysicalChanName" Type="Str">cDAQ-phase2012Mod3/ai8</Property>
				<Property Name="\9\AI.ADCTimingMode" Type="Str">High Resolution</Property>
				<Property Name="\9\AI.Current.Units" Type="Str">Amps</Property>
				<Property Name="\9\AI.CurrentShunt.Loc" Type="Str">Internal</Property>
				<Property Name="\9\AI.CurrentShunt.Resistance" Type="Str">34.009999999999998</Property>
				<Property Name="\9\AI.Max" Type="Str">0.021999999999999999</Property>
				<Property Name="\9\AI.MeasType" Type="Str">Current</Property>
				<Property Name="\9\AI.Min" Type="Str">0</Property>
				<Property Name="\9\AI.TermCfg" Type="Str">RSE</Property>
				<Property Name="\9\ChanType" Type="Str">Analog Input</Property>
				<Property Name="\9\Name" Type="Str">CurrentTask/Current_9</Property>
				<Property Name="\9\PhysicalChanName" Type="Str">cDAQ-phase2012Mod3/ai9</Property>
				<Property Name="Channels" Type="Str">CurrentTask/Current_0, CurrentTask/Current_1, CurrentTask/Current_2, CurrentTask/Current_3, CurrentTask/Current_4, CurrentTask/Current_5, CurrentTask/Current_6, CurrentTask/Current_7, CurrentTask/Current_8, CurrentTask/Current_9, CurrentTask/Current_10, CurrentTask/Current_11, CurrentTask/Current_12, CurrentTask/Current_13, CurrentTask/Current_14, CurrentTask/Current_15</Property>
				<Property Name="Name" Type="Str">CurrentTask</Property>
				<Property Name="SampTimingType" Type="Str">On Demand</Property>
			</Item>
			<Item Name="DigitalOutTask" Type="NI-DAQmx Task">
				<Property Name="\0\ChanType" Type="Str">Digital Output</Property>
				<Property Name="\0\DO.InvertLines" Type="Str">0</Property>
				<Property Name="\0\Name" Type="Str">DigitalOutTask/DigitalOut_0</Property>
				<Property Name="\0\PhysicalChanName" Type="Str">cDAQ-phase2012Mod2/port0/line0</Property>
				<Property Name="\1\ChanType" Type="Str">Digital Output</Property>
				<Property Name="\1\DO.InvertLines" Type="Str">0</Property>
				<Property Name="\1\Name" Type="Str">DigitalOutTask/DigitalOut_1</Property>
				<Property Name="\1\PhysicalChanName" Type="Str">cDAQ-phase2012Mod2/port0/line1</Property>
				<Property Name="\10\ChanType" Type="Str">Digital Output</Property>
				<Property Name="\10\DO.InvertLines" Type="Str">0</Property>
				<Property Name="\10\Name" Type="Str">DigitalOutTask/DigitalOut_10</Property>
				<Property Name="\10\PhysicalChanName" Type="Str">cDAQ-phase2012Mod2/port0/line10</Property>
				<Property Name="\11\ChanType" Type="Str">Digital Output</Property>
				<Property Name="\11\DO.InvertLines" Type="Str">0</Property>
				<Property Name="\11\Name" Type="Str">DigitalOutTask/DigitalOut_11</Property>
				<Property Name="\11\PhysicalChanName" Type="Str">cDAQ-phase2012Mod2/port0/line11</Property>
				<Property Name="\12\ChanType" Type="Str">Digital Output</Property>
				<Property Name="\12\DO.InvertLines" Type="Str">0</Property>
				<Property Name="\12\Name" Type="Str">DigitalOutTask/DigitalOut_12</Property>
				<Property Name="\12\PhysicalChanName" Type="Str">cDAQ-phase2012Mod2/port0/line12</Property>
				<Property Name="\13\ChanType" Type="Str">Digital Output</Property>
				<Property Name="\13\DO.InvertLines" Type="Str">0</Property>
				<Property Name="\13\Name" Type="Str">DigitalOutTask/DigitalOut_13</Property>
				<Property Name="\13\PhysicalChanName" Type="Str">cDAQ-phase2012Mod2/port0/line13</Property>
				<Property Name="\14\ChanType" Type="Str">Digital Output</Property>
				<Property Name="\14\DO.InvertLines" Type="Str">0</Property>
				<Property Name="\14\Name" Type="Str">DigitalOutTask/DigitalOut_14</Property>
				<Property Name="\14\PhysicalChanName" Type="Str">cDAQ-phase2012Mod2/port0/line14</Property>
				<Property Name="\15\ChanType" Type="Str">Digital Output</Property>
				<Property Name="\15\DO.InvertLines" Type="Str">0</Property>
				<Property Name="\15\Name" Type="Str">DigitalOutTask/DigitalOut_15</Property>
				<Property Name="\15\PhysicalChanName" Type="Str">cDAQ-phase2012Mod2/port0/line15</Property>
				<Property Name="\16\ChanType" Type="Str">Digital Output</Property>
				<Property Name="\16\DO.InvertLines" Type="Str">0</Property>
				<Property Name="\16\Name" Type="Str">DigitalOutTask/DigitalOut_16</Property>
				<Property Name="\16\PhysicalChanName" Type="Str">cDAQ-phase2012Mod2/port0/line16</Property>
				<Property Name="\17\ChanType" Type="Str">Digital Output</Property>
				<Property Name="\17\DO.InvertLines" Type="Str">0</Property>
				<Property Name="\17\Name" Type="Str">DigitalOutTask/DigitalOut_17</Property>
				<Property Name="\17\PhysicalChanName" Type="Str">cDAQ-phase2012Mod2/port0/line17</Property>
				<Property Name="\18\ChanType" Type="Str">Digital Output</Property>
				<Property Name="\18\DO.InvertLines" Type="Str">0</Property>
				<Property Name="\18\Name" Type="Str">DigitalOutTask/DigitalOut_18</Property>
				<Property Name="\18\PhysicalChanName" Type="Str">cDAQ-phase2012Mod2/port0/line18</Property>
				<Property Name="\19\ChanType" Type="Str">Digital Output</Property>
				<Property Name="\19\DO.InvertLines" Type="Str">0</Property>
				<Property Name="\19\Name" Type="Str">DigitalOutTask/DigitalOut_19</Property>
				<Property Name="\19\PhysicalChanName" Type="Str">cDAQ-phase2012Mod2/port0/line19</Property>
				<Property Name="\2\ChanType" Type="Str">Digital Output</Property>
				<Property Name="\2\DO.InvertLines" Type="Str">0</Property>
				<Property Name="\2\Name" Type="Str">DigitalOutTask/DigitalOut_2</Property>
				<Property Name="\2\PhysicalChanName" Type="Str">cDAQ-phase2012Mod2/port0/line2</Property>
				<Property Name="\20\ChanType" Type="Str">Digital Output</Property>
				<Property Name="\20\DO.InvertLines" Type="Str">0</Property>
				<Property Name="\20\Name" Type="Str">DigitalOutTask/DigitalOut_20</Property>
				<Property Name="\20\PhysicalChanName" Type="Str">cDAQ-phase2012Mod2/port0/line20</Property>
				<Property Name="\21\ChanType" Type="Str">Digital Output</Property>
				<Property Name="\21\DO.InvertLines" Type="Str">0</Property>
				<Property Name="\21\Name" Type="Str">DigitalOutTask/DigitalOut_21</Property>
				<Property Name="\21\PhysicalChanName" Type="Str">cDAQ-phase2012Mod2/port0/line21</Property>
				<Property Name="\22\ChanType" Type="Str">Digital Output</Property>
				<Property Name="\22\DO.InvertLines" Type="Str">0</Property>
				<Property Name="\22\Name" Type="Str">DigitalOutTask/DigitalOut_22</Property>
				<Property Name="\22\PhysicalChanName" Type="Str">cDAQ-phase2012Mod2/port0/line22</Property>
				<Property Name="\23\ChanType" Type="Str">Digital Output</Property>
				<Property Name="\23\DO.InvertLines" Type="Str">0</Property>
				<Property Name="\23\Name" Type="Str">DigitalOutTask/DigitalOut_23</Property>
				<Property Name="\23\PhysicalChanName" Type="Str">cDAQ-phase2012Mod2/port0/line23</Property>
				<Property Name="\24\ChanType" Type="Str">Digital Output</Property>
				<Property Name="\24\DO.InvertLines" Type="Str">0</Property>
				<Property Name="\24\Name" Type="Str">DigitalOutTask/DigitalOut_24</Property>
				<Property Name="\24\PhysicalChanName" Type="Str">cDAQ-phase2012Mod2/port0/line24</Property>
				<Property Name="\25\ChanType" Type="Str">Digital Output</Property>
				<Property Name="\25\DO.InvertLines" Type="Str">0</Property>
				<Property Name="\25\Name" Type="Str">DigitalOutTask/DigitalOut_25</Property>
				<Property Name="\25\PhysicalChanName" Type="Str">cDAQ-phase2012Mod2/port0/line25</Property>
				<Property Name="\26\ChanType" Type="Str">Digital Output</Property>
				<Property Name="\26\DO.InvertLines" Type="Str">0</Property>
				<Property Name="\26\Name" Type="Str">DigitalOutTask/DigitalOut_26</Property>
				<Property Name="\26\PhysicalChanName" Type="Str">cDAQ-phase2012Mod2/port0/line26</Property>
				<Property Name="\27\ChanType" Type="Str">Digital Output</Property>
				<Property Name="\27\DO.InvertLines" Type="Str">0</Property>
				<Property Name="\27\Name" Type="Str">DigitalOutTask/DigitalOut_27</Property>
				<Property Name="\27\PhysicalChanName" Type="Str">cDAQ-phase2012Mod2/port0/line27</Property>
				<Property Name="\3\ChanType" Type="Str">Digital Output</Property>
				<Property Name="\3\DO.InvertLines" Type="Str">0</Property>
				<Property Name="\3\Name" Type="Str">DigitalOutTask/DigitalOut_3</Property>
				<Property Name="\3\PhysicalChanName" Type="Str">cDAQ-phase2012Mod2/port0/line3</Property>
				<Property Name="\4\ChanType" Type="Str">Digital Output</Property>
				<Property Name="\4\DO.InvertLines" Type="Str">0</Property>
				<Property Name="\4\Name" Type="Str">DigitalOutTask/DigitalOut_4</Property>
				<Property Name="\4\PhysicalChanName" Type="Str">cDAQ-phase2012Mod2/port0/line4</Property>
				<Property Name="\5\ChanType" Type="Str">Digital Output</Property>
				<Property Name="\5\DO.InvertLines" Type="Str">0</Property>
				<Property Name="\5\Name" Type="Str">DigitalOutTask/DigitalOut_5</Property>
				<Property Name="\5\PhysicalChanName" Type="Str">cDAQ-phase2012Mod2/port0/line5</Property>
				<Property Name="\6\ChanType" Type="Str">Digital Output</Property>
				<Property Name="\6\DO.InvertLines" Type="Str">0</Property>
				<Property Name="\6\Name" Type="Str">DigitalOutTask/DigitalOut_6</Property>
				<Property Name="\6\PhysicalChanName" Type="Str">cDAQ-phase2012Mod2/port0/line6</Property>
				<Property Name="\7\ChanType" Type="Str">Digital Output</Property>
				<Property Name="\7\DO.InvertLines" Type="Str">0</Property>
				<Property Name="\7\Name" Type="Str">DigitalOutTask/DigitalOut_7</Property>
				<Property Name="\7\PhysicalChanName" Type="Str">cDAQ-phase2012Mod2/port0/line7</Property>
				<Property Name="\8\ChanType" Type="Str">Digital Output</Property>
				<Property Name="\8\DO.InvertLines" Type="Str">0</Property>
				<Property Name="\8\Name" Type="Str">DigitalOutTask/DigitalOut_8</Property>
				<Property Name="\8\PhysicalChanName" Type="Str">cDAQ-phase2012Mod2/port0/line8</Property>
				<Property Name="\9\ChanType" Type="Str">Digital Output</Property>
				<Property Name="\9\DO.InvertLines" Type="Str">0</Property>
				<Property Name="\9\Name" Type="Str">DigitalOutTask/DigitalOut_9</Property>
				<Property Name="\9\PhysicalChanName" Type="Str">cDAQ-phase2012Mod2/port0/line9</Property>
				<Property Name="Channels" Type="Str">DigitalOutTask/DigitalOut_0, DigitalOutTask/DigitalOut_1, DigitalOutTask/DigitalOut_2, DigitalOutTask/DigitalOut_3, DigitalOutTask/DigitalOut_4, DigitalOutTask/DigitalOut_5, DigitalOutTask/DigitalOut_6, DigitalOutTask/DigitalOut_7, DigitalOutTask/DigitalOut_8, DigitalOutTask/DigitalOut_9, DigitalOutTask/DigitalOut_10, DigitalOutTask/DigitalOut_11, DigitalOutTask/DigitalOut_12, DigitalOutTask/DigitalOut_13, DigitalOutTask/DigitalOut_14, DigitalOutTask/DigitalOut_15, DigitalOutTask/DigitalOut_16, DigitalOutTask/DigitalOut_17, DigitalOutTask/DigitalOut_18, DigitalOutTask/DigitalOut_19, DigitalOutTask/DigitalOut_20, DigitalOutTask/DigitalOut_21, DigitalOutTask/DigitalOut_22, DigitalOutTask/DigitalOut_23, DigitalOutTask/DigitalOut_24, DigitalOutTask/DigitalOut_25, DigitalOutTask/DigitalOut_26, DigitalOutTask/DigitalOut_27</Property>
				<Property Name="Name" Type="Str">DigitalOutTask</Property>
				<Property Name="SampTimingType" Type="Str">On Demand</Property>
			</Item>
			<Item Name="TemperatureTask" Type="NI-DAQmx Task">
				<Property Name="\0\AI.AutoZeroMode" Type="Str">Every Sample</Property>
				<Property Name="\0\AI.Max" Type="Str">150</Property>
				<Property Name="\0\AI.MeasType" Type="Str">Temperature:Thermocouple</Property>
				<Property Name="\0\AI.Min" Type="Str">0</Property>
				<Property Name="\0\AI.Temp.Units" Type="Str">Deg C</Property>
				<Property Name="\0\AI.Thrmcpl.CJCChan" Type="Str"></Property>
				<Property Name="\0\AI.Thrmcpl.CJCSrc" Type="Str">Built-In</Property>
				<Property Name="\0\AI.Thrmcpl.CJCVal" Type="Str">25</Property>
				<Property Name="\0\AI.Thrmcpl.Type" Type="Str">K</Property>
				<Property Name="\0\ChanType" Type="Str">Analog Input</Property>
				<Property Name="\0\Name" Type="Str">TemperatureTask/Temperature_0</Property>
				<Property Name="\0\PhysicalChanName" Type="Str">cDAQ-phase2012Mod1/ai0</Property>
				<Property Name="\1\AI.AutoZeroMode" Type="Str">Every Sample</Property>
				<Property Name="\1\AI.Max" Type="Str">150</Property>
				<Property Name="\1\AI.MeasType" Type="Str">Temperature:Thermocouple</Property>
				<Property Name="\1\AI.Min" Type="Str">0</Property>
				<Property Name="\1\AI.Temp.Units" Type="Str">Deg C</Property>
				<Property Name="\1\AI.Thrmcpl.CJCChan" Type="Str"></Property>
				<Property Name="\1\AI.Thrmcpl.CJCSrc" Type="Str">Built-In</Property>
				<Property Name="\1\AI.Thrmcpl.CJCVal" Type="Str">25</Property>
				<Property Name="\1\AI.Thrmcpl.Type" Type="Str">K</Property>
				<Property Name="\1\ChanType" Type="Str">Analog Input</Property>
				<Property Name="\1\Name" Type="Str">TemperatureTask/Temperature_1</Property>
				<Property Name="\1\PhysicalChanName" Type="Str">cDAQ-phase2012Mod1/ai1</Property>
				<Property Name="\10\AI.AutoZeroMode" Type="Str">Every Sample</Property>
				<Property Name="\10\AI.Max" Type="Str">150</Property>
				<Property Name="\10\AI.MeasType" Type="Str">Temperature:Thermocouple</Property>
				<Property Name="\10\AI.Min" Type="Str">0</Property>
				<Property Name="\10\AI.Temp.Units" Type="Str">Deg C</Property>
				<Property Name="\10\AI.Thrmcpl.CJCChan" Type="Str"></Property>
				<Property Name="\10\AI.Thrmcpl.CJCSrc" Type="Str">Built-In</Property>
				<Property Name="\10\AI.Thrmcpl.CJCVal" Type="Str">25</Property>
				<Property Name="\10\AI.Thrmcpl.Type" Type="Str">K</Property>
				<Property Name="\10\ChanType" Type="Str">Analog Input</Property>
				<Property Name="\10\Name" Type="Str">TemperatureTask/Temperature_10</Property>
				<Property Name="\10\PhysicalChanName" Type="Str">cDAQ-phase2012Mod1/ai10</Property>
				<Property Name="\11\AI.AutoZeroMode" Type="Str">Every Sample</Property>
				<Property Name="\11\AI.Max" Type="Str">150</Property>
				<Property Name="\11\AI.MeasType" Type="Str">Temperature:Thermocouple</Property>
				<Property Name="\11\AI.Min" Type="Str">0</Property>
				<Property Name="\11\AI.Temp.Units" Type="Str">Deg C</Property>
				<Property Name="\11\AI.Thrmcpl.CJCChan" Type="Str"></Property>
				<Property Name="\11\AI.Thrmcpl.CJCSrc" Type="Str">Built-In</Property>
				<Property Name="\11\AI.Thrmcpl.CJCVal" Type="Str">25</Property>
				<Property Name="\11\AI.Thrmcpl.Type" Type="Str">K</Property>
				<Property Name="\11\ChanType" Type="Str">Analog Input</Property>
				<Property Name="\11\Name" Type="Str">TemperatureTask/Temperature_11</Property>
				<Property Name="\11\PhysicalChanName" Type="Str">cDAQ-phase2012Mod1/ai11</Property>
				<Property Name="\12\AI.AutoZeroMode" Type="Str">Every Sample</Property>
				<Property Name="\12\AI.Max" Type="Str">150</Property>
				<Property Name="\12\AI.MeasType" Type="Str">Temperature:Thermocouple</Property>
				<Property Name="\12\AI.Min" Type="Str">0</Property>
				<Property Name="\12\AI.Temp.Units" Type="Str">Deg C</Property>
				<Property Name="\12\AI.Thrmcpl.CJCChan" Type="Str"></Property>
				<Property Name="\12\AI.Thrmcpl.CJCSrc" Type="Str">Built-In</Property>
				<Property Name="\12\AI.Thrmcpl.CJCVal" Type="Str">25</Property>
				<Property Name="\12\AI.Thrmcpl.Type" Type="Str">K</Property>
				<Property Name="\12\ChanType" Type="Str">Analog Input</Property>
				<Property Name="\12\Name" Type="Str">TemperatureTask/Temperature_12</Property>
				<Property Name="\12\PhysicalChanName" Type="Str">cDAQ-phase2012Mod1/ai12</Property>
				<Property Name="\13\AI.AutoZeroMode" Type="Str">Every Sample</Property>
				<Property Name="\13\AI.Max" Type="Str">150</Property>
				<Property Name="\13\AI.MeasType" Type="Str">Temperature:Thermocouple</Property>
				<Property Name="\13\AI.Min" Type="Str">0</Property>
				<Property Name="\13\AI.Temp.Units" Type="Str">Deg C</Property>
				<Property Name="\13\AI.Thrmcpl.CJCChan" Type="Str"></Property>
				<Property Name="\13\AI.Thrmcpl.CJCSrc" Type="Str">Built-In</Property>
				<Property Name="\13\AI.Thrmcpl.CJCVal" Type="Str">25</Property>
				<Property Name="\13\AI.Thrmcpl.Type" Type="Str">K</Property>
				<Property Name="\13\ChanType" Type="Str">Analog Input</Property>
				<Property Name="\13\Name" Type="Str">TemperatureTask/Temperature_13</Property>
				<Property Name="\13\PhysicalChanName" Type="Str">cDAQ-phase2012Mod1/ai13</Property>
				<Property Name="\14\AI.AutoZeroMode" Type="Str">Every Sample</Property>
				<Property Name="\14\AI.Max" Type="Str">150</Property>
				<Property Name="\14\AI.MeasType" Type="Str">Temperature:Thermocouple</Property>
				<Property Name="\14\AI.Min" Type="Str">0</Property>
				<Property Name="\14\AI.Temp.Units" Type="Str">Deg C</Property>
				<Property Name="\14\AI.Thrmcpl.CJCChan" Type="Str"></Property>
				<Property Name="\14\AI.Thrmcpl.CJCSrc" Type="Str">Built-In</Property>
				<Property Name="\14\AI.Thrmcpl.CJCVal" Type="Str">25</Property>
				<Property Name="\14\AI.Thrmcpl.Type" Type="Str">K</Property>
				<Property Name="\14\ChanType" Type="Str">Analog Input</Property>
				<Property Name="\14\Name" Type="Str">TemperatureTask/Temperature_14</Property>
				<Property Name="\14\PhysicalChanName" Type="Str">cDAQ-phase2012Mod1/ai14</Property>
				<Property Name="\15\AI.AutoZeroMode" Type="Str">Every Sample</Property>
				<Property Name="\15\AI.Max" Type="Str">150</Property>
				<Property Name="\15\AI.MeasType" Type="Str">Temperature:Thermocouple</Property>
				<Property Name="\15\AI.Min" Type="Str">0</Property>
				<Property Name="\15\AI.Temp.Units" Type="Str">Deg C</Property>
				<Property Name="\15\AI.Thrmcpl.CJCChan" Type="Str"></Property>
				<Property Name="\15\AI.Thrmcpl.CJCSrc" Type="Str">Built-In</Property>
				<Property Name="\15\AI.Thrmcpl.CJCVal" Type="Str">25</Property>
				<Property Name="\15\AI.Thrmcpl.Type" Type="Str">K</Property>
				<Property Name="\15\ChanType" Type="Str">Analog Input</Property>
				<Property Name="\15\Name" Type="Str">TemperatureTask/Temperature_15</Property>
				<Property Name="\15\PhysicalChanName" Type="Str">cDAQ-phase2012Mod1/ai15</Property>
				<Property Name="\2\AI.AutoZeroMode" Type="Str">Every Sample</Property>
				<Property Name="\2\AI.Max" Type="Str">150</Property>
				<Property Name="\2\AI.MeasType" Type="Str">Temperature:Thermocouple</Property>
				<Property Name="\2\AI.Min" Type="Str">0</Property>
				<Property Name="\2\AI.Temp.Units" Type="Str">Deg C</Property>
				<Property Name="\2\AI.Thrmcpl.CJCChan" Type="Str"></Property>
				<Property Name="\2\AI.Thrmcpl.CJCSrc" Type="Str">Built-In</Property>
				<Property Name="\2\AI.Thrmcpl.CJCVal" Type="Str">25</Property>
				<Property Name="\2\AI.Thrmcpl.Type" Type="Str">K</Property>
				<Property Name="\2\ChanType" Type="Str">Analog Input</Property>
				<Property Name="\2\Name" Type="Str">TemperatureTask/Temperature_2</Property>
				<Property Name="\2\PhysicalChanName" Type="Str">cDAQ-phase2012Mod1/ai2</Property>
				<Property Name="\3\AI.AutoZeroMode" Type="Str">Every Sample</Property>
				<Property Name="\3\AI.Max" Type="Str">150</Property>
				<Property Name="\3\AI.MeasType" Type="Str">Temperature:Thermocouple</Property>
				<Property Name="\3\AI.Min" Type="Str">0</Property>
				<Property Name="\3\AI.Temp.Units" Type="Str">Deg C</Property>
				<Property Name="\3\AI.Thrmcpl.CJCChan" Type="Str"></Property>
				<Property Name="\3\AI.Thrmcpl.CJCSrc" Type="Str">Built-In</Property>
				<Property Name="\3\AI.Thrmcpl.CJCVal" Type="Str">25</Property>
				<Property Name="\3\AI.Thrmcpl.Type" Type="Str">K</Property>
				<Property Name="\3\ChanType" Type="Str">Analog Input</Property>
				<Property Name="\3\Name" Type="Str">TemperatureTask/Temperature_3</Property>
				<Property Name="\3\PhysicalChanName" Type="Str">cDAQ-phase2012Mod1/ai3</Property>
				<Property Name="\4\AI.AutoZeroMode" Type="Str">Every Sample</Property>
				<Property Name="\4\AI.Max" Type="Str">150</Property>
				<Property Name="\4\AI.MeasType" Type="Str">Temperature:Thermocouple</Property>
				<Property Name="\4\AI.Min" Type="Str">0</Property>
				<Property Name="\4\AI.Temp.Units" Type="Str">Deg C</Property>
				<Property Name="\4\AI.Thrmcpl.CJCChan" Type="Str"></Property>
				<Property Name="\4\AI.Thrmcpl.CJCSrc" Type="Str">Built-In</Property>
				<Property Name="\4\AI.Thrmcpl.CJCVal" Type="Str">25</Property>
				<Property Name="\4\AI.Thrmcpl.Type" Type="Str">K</Property>
				<Property Name="\4\ChanType" Type="Str">Analog Input</Property>
				<Property Name="\4\Name" Type="Str">TemperatureTask/Temperature_4</Property>
				<Property Name="\4\PhysicalChanName" Type="Str">cDAQ-phase2012Mod1/ai4</Property>
				<Property Name="\5\AI.AutoZeroMode" Type="Str">Every Sample</Property>
				<Property Name="\5\AI.Max" Type="Str">150</Property>
				<Property Name="\5\AI.MeasType" Type="Str">Temperature:Thermocouple</Property>
				<Property Name="\5\AI.Min" Type="Str">0</Property>
				<Property Name="\5\AI.Temp.Units" Type="Str">Deg C</Property>
				<Property Name="\5\AI.Thrmcpl.CJCChan" Type="Str"></Property>
				<Property Name="\5\AI.Thrmcpl.CJCSrc" Type="Str">Built-In</Property>
				<Property Name="\5\AI.Thrmcpl.CJCVal" Type="Str">25</Property>
				<Property Name="\5\AI.Thrmcpl.Type" Type="Str">K</Property>
				<Property Name="\5\ChanType" Type="Str">Analog Input</Property>
				<Property Name="\5\Name" Type="Str">TemperatureTask/Temperature_5</Property>
				<Property Name="\5\PhysicalChanName" Type="Str">cDAQ-phase2012Mod1/ai5</Property>
				<Property Name="\6\AI.AutoZeroMode" Type="Str">Every Sample</Property>
				<Property Name="\6\AI.Max" Type="Str">150</Property>
				<Property Name="\6\AI.MeasType" Type="Str">Temperature:Thermocouple</Property>
				<Property Name="\6\AI.Min" Type="Str">0</Property>
				<Property Name="\6\AI.Temp.Units" Type="Str">Deg C</Property>
				<Property Name="\6\AI.Thrmcpl.CJCChan" Type="Str"></Property>
				<Property Name="\6\AI.Thrmcpl.CJCSrc" Type="Str">Built-In</Property>
				<Property Name="\6\AI.Thrmcpl.CJCVal" Type="Str">25</Property>
				<Property Name="\6\AI.Thrmcpl.Type" Type="Str">K</Property>
				<Property Name="\6\ChanType" Type="Str">Analog Input</Property>
				<Property Name="\6\Name" Type="Str">TemperatureTask/Temperature_6</Property>
				<Property Name="\6\PhysicalChanName" Type="Str">cDAQ-phase2012Mod1/ai6</Property>
				<Property Name="\7\AI.AutoZeroMode" Type="Str">Every Sample</Property>
				<Property Name="\7\AI.Max" Type="Str">150</Property>
				<Property Name="\7\AI.MeasType" Type="Str">Temperature:Thermocouple</Property>
				<Property Name="\7\AI.Min" Type="Str">0</Property>
				<Property Name="\7\AI.Temp.Units" Type="Str">Deg C</Property>
				<Property Name="\7\AI.Thrmcpl.CJCChan" Type="Str"></Property>
				<Property Name="\7\AI.Thrmcpl.CJCSrc" Type="Str">Built-In</Property>
				<Property Name="\7\AI.Thrmcpl.CJCVal" Type="Str">25</Property>
				<Property Name="\7\AI.Thrmcpl.Type" Type="Str">K</Property>
				<Property Name="\7\ChanType" Type="Str">Analog Input</Property>
				<Property Name="\7\Name" Type="Str">TemperatureTask/Temperature_7</Property>
				<Property Name="\7\PhysicalChanName" Type="Str">cDAQ-phase2012Mod1/ai7</Property>
				<Property Name="\8\AI.AutoZeroMode" Type="Str">Every Sample</Property>
				<Property Name="\8\AI.Max" Type="Str">150</Property>
				<Property Name="\8\AI.MeasType" Type="Str">Temperature:Thermocouple</Property>
				<Property Name="\8\AI.Min" Type="Str">0</Property>
				<Property Name="\8\AI.Temp.Units" Type="Str">Deg C</Property>
				<Property Name="\8\AI.Thrmcpl.CJCChan" Type="Str"></Property>
				<Property Name="\8\AI.Thrmcpl.CJCSrc" Type="Str">Built-In</Property>
				<Property Name="\8\AI.Thrmcpl.CJCVal" Type="Str">25</Property>
				<Property Name="\8\AI.Thrmcpl.Type" Type="Str">K</Property>
				<Property Name="\8\ChanType" Type="Str">Analog Input</Property>
				<Property Name="\8\Name" Type="Str">TemperatureTask/Temperature_8</Property>
				<Property Name="\8\PhysicalChanName" Type="Str">cDAQ-phase2012Mod1/ai8</Property>
				<Property Name="\9\AI.AutoZeroMode" Type="Str">Every Sample</Property>
				<Property Name="\9\AI.Max" Type="Str">150</Property>
				<Property Name="\9\AI.MeasType" Type="Str">Temperature:Thermocouple</Property>
				<Property Name="\9\AI.Min" Type="Str">0</Property>
				<Property Name="\9\AI.Temp.Units" Type="Str">Deg C</Property>
				<Property Name="\9\AI.Thrmcpl.CJCChan" Type="Str"></Property>
				<Property Name="\9\AI.Thrmcpl.CJCSrc" Type="Str">Built-In</Property>
				<Property Name="\9\AI.Thrmcpl.CJCVal" Type="Str">25</Property>
				<Property Name="\9\AI.Thrmcpl.Type" Type="Str">K</Property>
				<Property Name="\9\ChanType" Type="Str">Analog Input</Property>
				<Property Name="\9\Name" Type="Str">TemperatureTask/Temperature_9</Property>
				<Property Name="\9\PhysicalChanName" Type="Str">cDAQ-phase2012Mod1/ai9</Property>
				<Property Name="Channels" Type="Str">TemperatureTask/Temperature_0, TemperatureTask/Temperature_1, TemperatureTask/Temperature_2, TemperatureTask/Temperature_3, TemperatureTask/Temperature_4, TemperatureTask/Temperature_5, TemperatureTask/Temperature_6, TemperatureTask/Temperature_7, TemperatureTask/Temperature_8, TemperatureTask/Temperature_9, TemperatureTask/Temperature_10, TemperatureTask/Temperature_11, TemperatureTask/Temperature_12, TemperatureTask/Temperature_13, TemperatureTask/Temperature_14, TemperatureTask/Temperature_15</Property>
				<Property Name="Name" Type="Str">TemperatureTask</Property>
				<Property Name="SampTimingType" Type="Str">On Demand</Property>
			</Item>
		</Item>
		<Item Name="host cDAQ.vi" Type="VI" URL="../host cDAQ.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="Dflt Data Dir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Dflt Data Dir.vi"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="Write Spreadsheet String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Spreadsheet String.vi"/>
				<Item Name="Write Delimited Spreadsheet (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Delimited Spreadsheet (string).vi"/>
				<Item Name="Write Delimited Spreadsheet (I64).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Delimited Spreadsheet (I64).vi"/>
				<Item Name="Write Delimited Spreadsheet (DBL).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Delimited Spreadsheet (DBL).vi"/>
				<Item Name="Write Delimited Spreadsheet.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Delimited Spreadsheet.vi"/>
				<Item Name="DAQmx Write.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write.vi"/>
				<Item Name="DAQmx Write (Analog 1D DBL 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 1D DBL 1Chan NSamp).vi"/>
				<Item Name="DAQmx Fill In Error Info.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/miscellaneous.llb/DAQmx Fill In Error Info.vi"/>
				<Item Name="DAQmx Write (Analog 1D DBL NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 1D DBL NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Analog 1D Wfm NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 1D Wfm NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Analog 2D DBL NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 2D DBL NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Analog DBL 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog DBL 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Analog Wfm 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog Wfm 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Analog Wfm 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog Wfm 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 2D U32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 2D U32 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 2D U8 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 2D U8 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 1D Bool 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D Bool 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U32 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U32 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U8 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U8 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital Bool 1Line 1Point).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital Bool 1Line 1Point).vi"/>
				<Item Name="DAQmx Write (Digital Wfm 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital Wfm 1Chan NSamp).vi"/>
				<Item Name="DWDT Uncompress Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Uncompress Digital.vi"/>
				<Item Name="DTbl Uncompress Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Uncompress Digital.vi"/>
				<Item Name="DTbl Digital Size.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Digital Size.vi"/>
				<Item Name="DAQmx Write (Raw 1D I16).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D I16).vi"/>
				<Item Name="DAQmx Write (Raw 1D I32).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D I32).vi"/>
				<Item Name="DAQmx Write (Raw 1D I8).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D I8).vi"/>
				<Item Name="DAQmx Write (Raw 1D U16).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D U16).vi"/>
				<Item Name="DAQmx Write (Raw 1D U32).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D U32).vi"/>
				<Item Name="DAQmx Write (Raw 1D U8).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D U8).vi"/>
				<Item Name="DAQmx Write (Digital 1D Wfm NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D Wfm NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital Wfm 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital Wfm 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Analog 1D Wfm NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 1D Wfm NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 1D Wfm NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D Wfm NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital U8 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital U8 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital U32 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital U32 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U32 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U32 NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U8 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U8 NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 2D Bool NChan 1Samp NLine).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 2D Bool NChan 1Samp NLine).vi"/>
				<Item Name="DAQmx Write (Digital 1D Bool NChan 1Samp 1Line).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D Bool NChan 1Samp 1Line).vi"/>
				<Item Name="DAQmx Write (Analog 2D I16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 2D I16 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Analog 2D U16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 2D U16 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Counter Frequency 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter Frequency 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Counter Ticks 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter Ticks 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Counter Time 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter Time 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Counter 1D Frequency NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1D Frequency NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Counter 1D Time NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1D Time NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Counter 1DTicks NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1DTicks NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Analog 2D I32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 2D I32 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital U16 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital U16 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U16 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U16 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U16 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U16 NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 2D U16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 2D U16 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Counter 1D Frequency 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1D Frequency 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Counter 1D Ticks 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1D Ticks 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Counter 1D Time 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1D Time 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read.vi"/>
				<Item Name="DAQmx Read (Analog 1D Wfm NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 1D Wfm NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 1D DBL 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 1D DBL 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 1D DBL NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 1D DBL NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Analog 1D Wfm NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 1D Wfm NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Analog 2D DBL NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D DBL NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog DBL 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog DBL 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Analog Wfm 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog Wfm 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Analog Wfm 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog Wfm 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 1D Bool 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D Bool 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U32 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U32 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U8 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U8 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 1D Wfm NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D Wfm NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 2D U32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 2D U32 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 2D U8 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 2D U8 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital Bool 1Line 1Point).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital Bool 1Line 1Point).vi"/>
				<Item Name="DAQmx Read (Digital Wfm 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital Wfm 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Raw 1D I16).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D I16).vi"/>
				<Item Name="DAQmx Read (Raw 1D I32).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D I32).vi"/>
				<Item Name="DAQmx Read (Raw 1D I8).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D I8).vi"/>
				<Item Name="DAQmx Read (Raw 1D U16).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D U16).vi"/>
				<Item Name="DAQmx Read (Raw 1D U32).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D U32).vi"/>
				<Item Name="DAQmx Read (Raw 1D U8).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D U8).vi"/>
				<Item Name="DAQmx Read (Digital 1D Wfm NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D Wfm NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital Wfm 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital Wfm 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter 1D DBL 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D DBL 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter DBL 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter DBL 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter U32 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter U32 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter 1D U32 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D U32 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U8 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U8 NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U32 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U32 NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital U8 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital U8 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital U32 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital U32 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D Bool NChan 1Samp 1Line).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D Bool NChan 1Samp 1Line).vi"/>
				<Item Name="DAQmx Read (Digital 2D Bool NChan 1Samp NLine).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 2D Bool NChan 1Samp NLine).vi"/>
				<Item Name="DAQmx Read (Analog 2D U16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D U16 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 2D I16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D I16 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 2D I32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D I32 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 2D U32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D U32 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital U16 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital U16 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U16 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U16 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U16 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U16 NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 2D U16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 2D U16 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D Pulse Freq 1 Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D Pulse Freq 1 Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D Pulse Ticks 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D Pulse Ticks 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D Pulse Time 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D Pulse Time 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter Pulse Freq 1 Chan 1 Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter Pulse Freq 1 Chan 1 Samp).vi"/>
				<Item Name="DAQmx Read (Counter Pulse Ticks 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter Pulse Ticks 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter Pulse Time 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter Pulse Time 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter 1D DBL NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D DBL NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter 1D U32 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D U32 NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter 2D DBL NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 2D DBL NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 2D U32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 2D U32 NChan NSamp).vi"/>
				<Item Name="DAQmx Clear Task.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/task.llb/DAQmx Clear Task.vi"/>
				<Item Name="Find First Error.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find First Error.vi"/>
				<Item Name="Close File+.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Close File+.vi"/>
				<Item Name="compatReadText.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatReadText.vi"/>
				<Item Name="Read File+ (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read File+ (string).vi"/>
				<Item Name="Open File+.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Open File+.vi"/>
				<Item Name="Read Lines From File (with error IO).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read Lines From File (with error IO).vi"/>
				<Item Name="Read Delimited Spreadsheet (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read Delimited Spreadsheet (string).vi"/>
				<Item Name="Read Delimited Spreadsheet (I64).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read Delimited Spreadsheet (I64).vi"/>
				<Item Name="Read Delimited Spreadsheet (DBL).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read Delimited Spreadsheet (DBL).vi"/>
				<Item Name="Read Delimited Spreadsheet.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read Delimited Spreadsheet.vi"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="DAQmx Read (Analog Wfm 1Chan NSamp Duration).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog Wfm 1Chan NSamp Duration).vi"/>
				<Item Name="DAQmx Read (Analog 1D Wfm NChan NSamp Duration).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 1D Wfm NChan NSamp Duration).vi"/>
				<Item Name="DAQmx Read (Digital Wfm 1Chan NSamp Duration).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital Wfm 1Chan NSamp Duration).vi"/>
				<Item Name="DAQmx Read (Digital 1D Wfm NChan NSamp Duration).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D Wfm NChan NSamp Duration).vi"/>
			</Item>
			<Item Name="nilvaiu.dll" Type="Document" URL="nilvaiu.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="CoreFlood cDAQ" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{46C2EBEB-DBF7-43BF-9F25-6E2A626D1360}</Property>
				<Property Name="App_INI_GUID" Type="Str">{8922DD30-6E86-4F3B-A265-37DAC0FF7C5A}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="App_showHWConfig" Type="Bool">true</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{C0D220FA-DE61-4A00-B56D-B753D06EABAC}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">CoreFlood cDAQ</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToProject</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{27D3BF19-079C-4B58-8C94-B4B43BB56F83}</Property>
				<Property Name="Bld_version.build" Type="Int">39</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">CoreFlood_cDAQ.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/CoreFlood_cDAQ.exe</Property>
				<Property Name="Destination[0].path.type" Type="Str">relativeToProject</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/data</Property>
				<Property Name="Destination[1].path.type" Type="Str">relativeToProject</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{73CA7D84-308E-4E0F-8DEC-607800FF91C4}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/host cDAQ.vi</Property>
				<Property Name="Source[1].properties[0].type" Type="Str">Show Abort button</Property>
				<Property Name="Source[1].properties[0].value" Type="Bool">false</Property>
				<Property Name="Source[1].properties[1].type" Type="Str">Show toolbar</Property>
				<Property Name="Source[1].properties[1].value" Type="Bool">false</Property>
				<Property Name="Source[1].properties[2].type" Type="Str">Show horizontal scroll bar</Property>
				<Property Name="Source[1].properties[2].value" Type="Bool">false</Property>
				<Property Name="Source[1].properties[3].type" Type="Str">Show vertical scroll bar</Property>
				<Property Name="Source[1].properties[3].value" Type="Bool">false</Property>
				<Property Name="Source[1].properties[4].type" Type="Str">Show menu bar</Property>
				<Property Name="Source[1].properties[4].value" Type="Bool">false</Property>
				<Property Name="Source[1].propertiesCount" Type="Int">5</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_companyName" Type="Str">University of Wyoming</Property>
				<Property Name="TgtF_internalName" Type="Str">Core Flood cDAQ</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2017</Property>
				<Property Name="TgtF_productName" Type="Str">Core Flooding cDAQ</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{1C9C7517-B5AB-4320-A626-2401086E6B74}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">CoreFlood_cDAQ.exe</Property>
			</Item>
			<Item Name="Installer-cDAQ" Type="Installer">
				<Property Name="Destination[0].name" Type="Str">core flood</Property>
				<Property Name="Destination[0].parent" Type="Str">{3912416A-D2E5-411B-AFEE-B63654D690C0}</Property>
				<Property Name="Destination[0].tag" Type="Str">{1ADA0813-1AF7-4CAB-9BE9-D3645A5DF35B}</Property>
				<Property Name="Destination[0].type" Type="Str">userFolder</Property>
				<Property Name="DestinationCount" Type="Int">1</Property>
				<Property Name="DistPart[0].flavorID" Type="Str">DefaultFull</Property>
				<Property Name="DistPart[0].productID" Type="Str">{3B7FDD1A-EB93-4986-B111-7BBCC8D3CC28}</Property>
				<Property Name="DistPart[0].productName" Type="Str">NI LabVIEW Runtime 2018 f1</Property>
				<Property Name="DistPart[0].SoftDep[0].exclude" Type="Bool">true</Property>
				<Property Name="DistPart[0].SoftDep[0].productName" Type="Str">NI ActiveX Container</Property>
				<Property Name="DistPart[0].SoftDep[0].upgradeCode" Type="Str">{1038A887-23E1-4289-B0BD-0C4B83C6BA21}</Property>
				<Property Name="DistPart[0].SoftDep[1].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[1].productName" Type="Str">Math Kernel Libraries 2017</Property>
				<Property Name="DistPart[0].SoftDep[1].upgradeCode" Type="Str">{699C1AC5-2CF2-4745-9674-B19536EBA8A3}</Property>
				<Property Name="DistPart[0].SoftDep[2].exclude" Type="Bool">true</Property>
				<Property Name="DistPart[0].SoftDep[2].productName" Type="Str">NI Logos 18.0</Property>
				<Property Name="DistPart[0].SoftDep[2].upgradeCode" Type="Str">{5E4A4CE3-4D06-11D4-8B22-006008C16337}</Property>
				<Property Name="DistPart[0].SoftDep[3].exclude" Type="Bool">true</Property>
				<Property Name="DistPart[0].SoftDep[3].productName" Type="Str">NI TDM Streaming 18.0</Property>
				<Property Name="DistPart[0].SoftDep[3].upgradeCode" Type="Str">{4CD11BE6-6BB7-4082-8A27-C13771BC309B}</Property>
				<Property Name="DistPart[0].SoftDep[4].exclude" Type="Bool">true</Property>
				<Property Name="DistPart[0].SoftDep[4].productName" Type="Str">NI LabVIEW Web Server 2018</Property>
				<Property Name="DistPart[0].SoftDep[4].upgradeCode" Type="Str">{0960380B-EA86-4E0C-8B57-14CD8CCF2C15}</Property>
				<Property Name="DistPart[0].SoftDep[5].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[5].productName" Type="Str">NI VC2010MSMs</Property>
				<Property Name="DistPart[0].SoftDep[5].upgradeCode" Type="Str">{EFBA6F9E-F934-4BD7-AC51-60CCA480489C}</Property>
				<Property Name="DistPart[0].SoftDep[6].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[6].productName" Type="Str">NI VC2015 Runtime</Property>
				<Property Name="DistPart[0].SoftDep[6].upgradeCode" Type="Str">{D42E7BAE-6589-4570-B6A3-3E28889392E7}</Property>
				<Property Name="DistPart[0].SoftDep[7].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[7].productName" Type="Str">NI mDNS Responder 17.0</Property>
				<Property Name="DistPart[0].SoftDep[7].upgradeCode" Type="Str">{9607874B-4BB3-42CB-B450-A2F5EF60BA3B}</Property>
				<Property Name="DistPart[0].SoftDep[8].exclude" Type="Bool">true</Property>
				<Property Name="DistPart[0].SoftDep[8].productName" Type="Str">NI Deployment Framework 2018</Property>
				<Property Name="DistPart[0].SoftDep[8].upgradeCode" Type="Str">{838942E4-B73C-492E-81A3-AA1E291FD0DC}</Property>
				<Property Name="DistPart[0].SoftDep[9].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[9].productName" Type="Str">NI Error Reporting 2018</Property>
				<Property Name="DistPart[0].SoftDep[9].upgradeCode" Type="Str">{42E818C6-2B08-4DE7-BD91-B0FD704C119A}</Property>
				<Property Name="DistPart[0].SoftDepCount" Type="Int">10</Property>
				<Property Name="DistPart[0].upgradeCode" Type="Str">{3B195EBF-4A09-46E6-8EAD-931568C1344C}</Property>
				<Property Name="DistPart[1].flavorID" Type="Str">_full_</Property>
				<Property Name="DistPart[1].productID" Type="Str">{C2B5E2FC-C98F-4AB6-AABD-F976D16698D3}</Property>
				<Property Name="DistPart[1].productName" Type="Str">NI-DAQmx Runtime with Configuration Support 17.1</Property>
				<Property Name="DistPart[1].upgradeCode" Type="Str">{9856368A-ED47-4944-87BE-8EF3472AE39B}</Property>
				<Property Name="DistPartCount" Type="Int">2</Property>
				<Property Name="INST_autoIncrement" Type="Bool">true</Property>
				<Property Name="INST_buildLocation" Type="Path">/C/Users/mperry1/Dropbox (UW DRS)/SW Distrib/Piri/core flood cDAQ</Property>
				<Property Name="INST_buildSpecName" Type="Str">Installer-cDAQ</Property>
				<Property Name="INST_defaultDir" Type="Str">{1ADA0813-1AF7-4CAB-9BE9-D3645A5DF35B}</Property>
				<Property Name="INST_productName" Type="Str">core flood cDAQ</Property>
				<Property Name="INST_productVersion" Type="Str">1.0.12</Property>
				<Property Name="InstSpecBitness" Type="Str">32-bit</Property>
				<Property Name="InstSpecVersion" Type="Str">18008011</Property>
				<Property Name="MSI_arpCompany" Type="Str">University of Wyoming</Property>
				<Property Name="MSI_arpContact" Type="Str">Marvin Perry</Property>
				<Property Name="MSI_arpPhone" Type="Str">(307) 760-7554</Property>
				<Property Name="MSI_arpURL" Type="Str">www.uwyo.edu/DRS</Property>
				<Property Name="MSI_bannerImageID" Type="Ref">/My Computer/logos/labview-578x59-v2.bmp</Property>
				<Property Name="MSI_distID" Type="Str">{505C07CD-1A43-4BED-8700-79C4F60FFA99}</Property>
				<Property Name="MSI_osCheck" Type="Int">0</Property>
				<Property Name="MSI_upgradeCode" Type="Str">{2F495030-E26D-47FD-9159-44C2B24630CD}</Property>
				<Property Name="MSI_welcomeImageID" Type="Ref">/My Computer/logos/labview-578x383-v4.bmp</Property>
				<Property Name="MSI_windowMessage" Type="Str">This will install data acquisition and valve control programs for only the cDAQ chassis. This system currently resides in EN 1007.  </Property>
				<Property Name="MSI_windowTitle" Type="Str">Core Flood DAQ interface</Property>
				<Property Name="RegDest[0].dirName" Type="Str">Software</Property>
				<Property Name="RegDest[0].dirTag" Type="Str">{DDFAFC8B-E728-4AC8-96DE-B920EBB97A86}</Property>
				<Property Name="RegDest[0].parentTag" Type="Str">2</Property>
				<Property Name="RegDestCount" Type="Int">1</Property>
				<Property Name="Source[0].dest" Type="Str">{1ADA0813-1AF7-4CAB-9BE9-D3645A5DF35B}</Property>
				<Property Name="Source[0].File[0].dest" Type="Str">{1ADA0813-1AF7-4CAB-9BE9-D3645A5DF35B}</Property>
				<Property Name="Source[0].File[0].name" Type="Str">CoreFlood_cDAQ.exe</Property>
				<Property Name="Source[0].File[0].Shortcut[0].destIndex" Type="Int">0</Property>
				<Property Name="Source[0].File[0].Shortcut[0].name" Type="Str">CoreFlood_cDAQ</Property>
				<Property Name="Source[0].File[0].Shortcut[0].subDir" Type="Str">core flood cDAQ</Property>
				<Property Name="Source[0].File[0].ShortcutCount" Type="Int">1</Property>
				<Property Name="Source[0].File[0].tag" Type="Str">{1C9C7517-B5AB-4320-A626-2401086E6B74}</Property>
				<Property Name="Source[0].FileCount" Type="Int">1</Property>
				<Property Name="Source[0].name" Type="Str">CoreFlood cDAQ</Property>
				<Property Name="Source[0].tag" Type="Ref">/My Computer/Build Specifications/CoreFlood cDAQ</Property>
				<Property Name="Source[0].type" Type="Str">EXE</Property>
				<Property Name="Source[1].File[0].dest" Type="Str">{1ADA0813-1AF7-4CAB-9BE9-D3645A5DF35B}</Property>
				<Property Name="Source[1].File[0].name" Type="Str">CoreFlood_cRIO.exe</Property>
				<Property Name="Source[1].File[0].tag" Type="Str">{62DD9167-0D43-4305-B7D6-79A614F6B86F}</Property>
				<Property Name="SourceCount" Type="Int">1</Property>
			</Item>
		</Item>
	</Item>
</Project>
